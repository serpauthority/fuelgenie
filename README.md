# Technology and Service Delivery

Technology is continuously changing to bring new enhancements, automation and innovative solutions to help solve the day to day issues consumers and businesses face. This has been very apparent in the service delivery industry. Many interactions that were once handled by a telephone operator or face to face, have now become automated or moved online.

Technology has had a significant impact on service delivery, this article looks at some of the key changes and their impact whilst also considering the issues with those left behind.

## Self service

Self service has become a substantial part of many interactions consumers have with companies on a day to day basis. Supermarket self service checkouts are one of the most prominent examples of how self service has altered the in store experience. Petrol stations have the option of self-service along with self check-in flights, self ordering in restaurants, self service tickets at the cinema and theaters, self service post office services and many more.

Each of these self service options changes the way we interact on a daily basis, removing many of the previously necessary human interaction and replacing these with machines. This is highly beneficial to many consumers, it’s more convenient and often quicker with fewer ques and waiting times. Businesses also benefit from the reduced overheads self service automation can bring, less staff often means lower prices for consumers and investment in additional technology.

However some consumers may not be happy to transition to self service and may still be uncomfortable or unable to use the machines that are in place. Keeping this in mind businesses need to cater for these customers by providing an alternative which still enables them to continue to purchase from the company. Failing to do this may alienate customers and result in them switching to a competitor who does offer an alternative.

## Online account management

Management of services is another key area where technology has changed how consumers both personal and businesses control and manage their; subscriptions, banking, energy, taxes, insurance, <span style="text-decoration: underline;">_**[business fuel cards](https://www.fuelgenie.co.uk/fuel-cards/company-fuel-cards/how-does-a-company-fuel-card-work/)**_</span>, rent, mortgages and many more products and services. Businesses now offer the ability to make changes, update details, gain quotes, make claims and apply for these services online, without needing to speak to a person face to face or over the phone.

This change has made it much easier for consumers to switch services in order to save money and benefit from new customer offers. Businesses have also been able to save costs by reducing the amount of call center operatives and physical stores needed to deliver these account management services.

## Online chat boxes

Online chat boxes have now become a staple in many businesses as a way to minimise the need for call centers and redirect queries to the appropriate answer without human interaction.

Automated chat bots allow businesses to collect information about the issue or question the user is having which saves time. These interactions would have previously been carried out over the phone during a one to one phone call. Online chat boxes also allow not only users but the operative to deal with multiple tasks at once. Operatives can answer several queries at one time and users can perform other tasks whilst they are waiting for an answer.

Businesses need to make sure there is a balance between the automated services and getting into contact with a real human operative. Customers or potential customers can become very frustrated if they need to speak to someone and cannot get past the automated questions. This poor user experience can result in customers switching to alternative providers and potentially submitting negative reviews which can be harmful for business growth.

## Consumer data

Consumer data is another key element of technology and service delivery, the machines used to provide self service and online account management collect data which can be used to personalise these services. As technology has evolved these machines and programmes have become more sophisticated in interpreting the data that is received. This means more personalised options for consumers including offers, deals and related product suggestions.

Technology has a huge part to play in current service delivery and the service delivery of the future. As we humans become more used to interacting with machines and automated services, they will continue to spread. Businesses however, need to be mindful of accessibility issues when removing human interaction from their service delivery to avoid alienating part of the population.

### Resources:

*   [Fuel Card - Beijing University](https://gitlab.educg.net/serpauthority/fuelgenie)
*   [Fuel Cards - Code Dream](https://git.codesdream.com/serpauthority/fuelgenie)
*   [Company Fuel Cards - GNOME](https://gitlab.gnome.org/serpauthority/fuelgenie)
